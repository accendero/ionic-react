export default {
    credentials: btoa('<username>:<api_key>'),
    url: '<jira_host>/rest/api/2/search?jql=assignee%20%3D%20currentUser()%20AND%20resolution%20%3D%20Unresolved%20ORDER%20BY%20updated%20DESC'
}
