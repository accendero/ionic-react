import {
    IonButtons,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewWillEnter
} from '@ionic/react';
import React, {useState} from 'react';
import {useHistory, useParams } from 'react-router';

import api from '../jira_api'
import {
    Issue, Issue_JSON
} from '../IssuesInterface'
import ListPage from "./List";
import DetailPage from "./Detail";

const IssuesPage: React.FC = () => {
    const [issues, setIssues] = useState<Issue[]>([]);
    const history = useHistory();
    const params = useParams();
    const showDetail = params.hasOwnProperty('id');

    useIonViewWillEnter(async () => {
        const result = await fetch(api.url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Basic ${api.credentials}`,
            },
        });
        const data = await result.json();
        const issues = data.issues.map((issue: Issue_JSON) => ({
            'id': issue.id,
            'key': issue.key,
            'url': issue.self,
            'summary': issue.fields.summary,
            'status': issue.fields.status.name,
            'project': {
                'url': issue.fields.project.self,
                'name': issue.fields.project.name,
                'key': issue.fields.project.key,
            }
        }));
        setIssues(issues);
    });
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Issues</IonTitle>
                </IonToolbar>
            </IonHeader>
             {showDetail ? <DetailPage issues={issues}/> :
                 <ListPage issues={issues} history={history} />}
        </IonPage>
    );
};

export default IssuesPage;
