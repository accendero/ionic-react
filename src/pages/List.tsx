import {
    IonBadge,
    IonContent,
    IonItem,
    IonLabel,
    IonList,
} from '@ionic/react';
import React from 'react';
import {History} from 'history';

import {Issue} from '../IssuesInterface';

interface ListPageProps {
    issues: Issue[],
    history: History
}

const ListPage: React.FC<ListPageProps> = ({issues, history}) => {
    const navigateToIssue = (s: string) => () => {
        history.push(`/home/issues/${s}`)
    };

  return (
      <IonContent>
          <IonList>
              {issues.map((issue, idx) =>
                  <IssueItem
                      key={idx}
                      issue={issue}
                      navFunc={navigateToIssue}
                  />
              )}
          </IonList>
      </IonContent>
  );
};

const IssueItem: React.FC<{ issue: Issue, navFunc: (s: string) => any }> =
    ({issue, navFunc}) => {
        return (
            <IonItem onClick={navFunc(issue.id)}>
                <IonBadge slot="start">{issue.key}</IonBadge>
                <IonLabel>
                    <p>{issue.status}</p>
                </IonLabel>
            </IonItem>
        );
};

export default ListPage;
