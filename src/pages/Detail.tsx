import React from "react";
import {
    IonBadge,
    IonContent,
    IonItem,
    IonCard,
    IonCardContent,
    IonCardHeader,
} from "@ionic/react";
import { useParams } from "react-router-dom";

import {Issue} from '../IssuesInterface';

interface Params {
    id: string
}


const DetailPage: React.FC<{issues: Issue[]}> = ({issues}) => {
    const params = useParams<Params>();
    const {id} = params;
    const issue: Issue = issues.filter(i => i.id === id)[0];
    return (
        <IonContent>
            {issue ?
                <IonCard>
                    <IonCardHeader>
                        <IonBadge>{issue.key}</IonBadge>
                    </IonCardHeader>
                    <IonCardContent>
                        <h2>{issue.project.name} - {issue.project.key}</h2>
                        <p>Status: {issue.status}</p>
                        <p>Summary: {issue.summary}</p>
                    </IonCardContent>
                </IonCard>
             :
                <IonItem>Issue Not Found</IonItem>
            }
        </IonContent>
    );
};

export default DetailPage
