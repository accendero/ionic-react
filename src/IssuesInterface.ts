
export interface Issue {
    id: string
    key: string;
    url: string;
    summary: string;
    status: string;
    project: Project;
}

export interface Project {
    url: string;
    key: string;
    name: string;
}

export interface Project_JSON {
    self: string;
    key: string;
    name: string;
}

export interface Issue_JSON {
    id: string
    key: string;
    self: string;
    fields: Fields_JSON;
}

export interface Fields_JSON {
    summary: string,
    status: Status_JSON,
    project: Project_JSON
}

export interface Status_JSON {
    name: string,
    self: string,
    description: string,
}